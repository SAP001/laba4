#include "operation.h"
#include <vector>
#include "spinoperation.h"
#include "movingooeration.h"
#include <cmath>
#include "pointnormalization.h"


#define KOF_NORM 30
#define KOF_SCALE 1.1

point* loadFile(std::string fileName, int* kofNorm)
{
    point *model=new point[400];
    std::ifstream file;
    file.open(fileName);
    std::string str;

    char* cStr= new char[255];
    char *chCount;

    for(int i=0; i < ROW_COUNT; i++)
    {
        getline(file,str);
        strcpy(cStr,str.c_str());
        chCount = strtok (cStr,",");
        for(int j = 0; j < COL_COUNT; j++)
        {
            model[i*20+j].z=std::atoi(chCount);
            model[i*20+j].x=(j+1);
            model[i*20+j].y=(i+1);
            chCount = strtok (NULL,",");
        }

    }
    model = getNormalizationModel(model, kofNorm);


    delete [] cStr;
    return model;
}

point* getSpinModel(coordinat axis, sides side, point* model)
{
    point* result;
    switch (axis)
    {
    case axX:
        result = getSpinModelOnX(side, model);
        break;
    case axY:
        result = getSpinModelOnY(side, model);
        break;
    case axZ:
        result = getSpinModelOnZ(side, model);
        break;
    }
    return result;
}


point *getMovingModel(coordinat axis, sides side, point *model)
{
    point* result;
    switch (axis)
    {
    case axX:
        result = getMovingModelOnX(side, model);
        break;
    case axY:
        result = getMovingModelOnY(side, model);
        break;
    case axZ:
        result = getMovingModelOnZ(side, model);
        break;
    }
    return result;
}

point *getScaleModel(toScale scale, point *model)
{
    point* resultModel = new point[400];
    int factor;
    if(scale == minus)
        factor = -1;
    else
        factor = 1;

    for(int i=0; i < ROW_COUNT; i++)
        for(int j = 0; j < COL_COUNT; j++)
        {
            resultModel[i*20+j].x = model[i*20+j].x*pow(KOF_SCALE,factor);
            resultModel[i*20+j].y = model[i*20+j].y*pow(KOF_SCALE,factor);
            resultModel[i*20+j].z = model[i*20+j].z*pow(KOF_SCALE,factor);
        }
    return resultModel;
}
