#ifndef MOVINGOOERATION_H
#define MOVINGOOERATION_H
#include "exectoperation.h"

point* getMovingModelOnX(sides side,point* model);
point* getMovingModelOnY(sides side,point* model);
point* getMovingModelOnZ(sides side,point* model);



#endif // MOVINGOOERATION_H
