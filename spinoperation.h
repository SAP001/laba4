#ifndef SPINOPERATION_H
#define SPINOPERATION_H
#include <exectoperation.h>



point* getSpinModelOnX(sides side,point* model);
point* getSpinModelOnY(sides side,point* model);
point* getSpinModelOnZ(sides side,point* model);


#endif // SPINOPERATION_H
