#include "spinoperation.h"
#include <cmath>

point* getSpinModelOnX(sides side,point* model)
{
    point* resultModel = new point[400];
    int factor;
    if(side == left)
        factor = -1;
    else
        factor = 1;

    for(int i=0; i < ROW_COUNT; i++)
        for(int j = 0; j < COL_COUNT; j++)
        {
            resultModel[i*20+j].x = model[i*20+j].x;
            resultModel[i*20+j].y = model[i*20+j].y*cos(SPIN_CORNER*factor)-model[i*20+j].z*sin(SPIN_CORNER*factor);
            resultModel[i*20+j].z = model[i*20+j].y*sin(SPIN_CORNER*factor)+model[i*20+j].z*cos(SPIN_CORNER*factor);
        }
    return resultModel;
}

point *getSpinModelOnY(sides side, point *model)
{
    point* resultModel = new point[400];
    int factor;
    if(side == left)
        factor = -1;
    else
        factor = 1;

    for(int i=0; i < ROW_COUNT; i++)
        for(int j = 0; j < COL_COUNT; j++)
        {
            resultModel[i*20+j].x = model[i*20+j].x*cos(SPIN_CORNER*factor)+model[i*20+j].z*sin(SPIN_CORNER*factor);
            resultModel[i*20+j].y = model[i*20+j].y;
            resultModel[i*20+j].z = (-1)*model[i*20+j].x*sin(SPIN_CORNER*factor)+model[i*20+j].z*cos(SPIN_CORNER*factor);
        }
    return resultModel;
}

point *getSpinModelOnZ(sides side, point *model)
{
    point* resultModel = new point[400];
    int factor;
    if(side == left)
        factor = -1;
    else
        factor = 1;

    for(int i=0; i < ROW_COUNT; i++)
        for(int j = 0; j < COL_COUNT; j++)
        {
            resultModel[i*20+j].x = model[i*20+j].x*cos(SPIN_CORNER*factor)-model[i*20+j].y*sin(SPIN_CORNER*factor);
            resultModel[i*20+j].y = model[i*20+j].x*sin(SPIN_CORNER*factor)+model[i*20+j].y*cos(SPIN_CORNER*factor);
            resultModel[i*20+j].z = model[i*20+j].z;
        }
    return resultModel;
}
