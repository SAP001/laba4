#include "movingooeration.h"



point *getMovingModelOnX(sides side, point *model)
{
    point* resultModel = new point[400];
    int factor;
    if(side == left)
        factor = -1;
    else
        factor = 1;

    for(int i=0; i < 20; i++)
        for(int j = 0; j < 20; j++)
        {
            resultModel[i*20+j].x = model[i*20+j].x + MOVING_STEP*factor;
            resultModel[i*20+j].y = model[i*20+j].y;
            resultModel[i*20+j].z = model[i*20+j].z;
        }
    return resultModel;
}

point *getMovingModelOnY(sides side, point *model)
{
    point* resultModel = new point[400];
    int factor;
    if(side == left)
        factor = -1;
    else
        factor = 1;

    for(int i=0; i < 20; i++)
        for(int j = 0; j < 20; j++)
        {
            resultModel[i*20+j].x = model[i*20+j].x;
            resultModel[i*20+j].y = model[i*20+j].y + MOVING_STEP*factor;
            resultModel[i*20+j].z = model[i*20+j].z;
        }
    return resultModel;
}

point *getMovingModelOnZ(sides side, point *model)
{
    point* resultModel = new point[400];
    int factor;
    if(side == left)
        factor = -1;
    else
        factor = 1;

    for(int i=0; i < 20; i++)
        for(int j = 0; j < 20; j++)
        {
            resultModel[i*20+j].x = model[i*20+j].x;
            resultModel[i*20+j].y = model[i*20+j].y;
            resultModel[i*20+j].z = model[i*20+j].z + MOVING_STEP*factor;
        }
    return resultModel;
}
