#include "exectoperation.h"
#include <operation.h>

resultDate exectOperation(operationType operation, sourceDate& source)
{
   resultDate result;
   switch (operation)
   {
   case getMasPoints:
       result.model = loadFile(source.fileName,source.kofNorm);
       break;
   case spinMod:
       result.model = getSpinModel(source.axis, source.side, source.model);
       break;
   case movingMod:
       result.model = getMovingModel(source.axis, source.side, source.model);
       break;
   case scalMod:
       result.model = getScaleModel(source.scale,source.model);
       break;
   }
   return result;
}
