#ifndef OPERATION_H
#define OPERATION_H
#include <iostream>
#include <fstream>
#include <cstring>
#include "exectoperation.h"



point* loadFile(std::string fileName, int* kofNorm);
point* getSpinModel(coordinat axis, sides side, point* model);
point* getMovingModel(coordinat axis, sides side, point* model);
point* getScaleModel(toScale scale,point* model);
#endif // OPERATION_H
