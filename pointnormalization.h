#ifndef POINTNORMALIZATION_H
#define POINTNORMALIZATION_H
#include "exectoperation.h"
#define MAX_X 20
#define MAX_Y 20
#define MIN_X 1
#define MIN_Y 1

point* getNormalizationModel(point* model, int * kofNorm);

#endif // POINTNORMALIZATION_H
