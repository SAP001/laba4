#include "pointnormalization.h"



point* getNormalizationModel(point *model, int * kofNorm)
{
    point* resMod =new point[400];
    int maxCount = model[0].z, minCount = model[0].z;

    for(int i=1;i < COL_COUNT * ROW_COUNT;i++)
    {
        if(model[i].z > maxCount)
            maxCount = model[i].z;
        else if (model[i].z < minCount)
            minCount = model[i].z;
    }
    for(int i = 0; i < COL_COUNT * ROW_COUNT; i++)
    {
        resMod[i].x = kofNorm[0] + (model[i].x - MIN_X)*(kofNorm[1] - kofNorm[0])/(MAX_X - MIN_X);
        resMod[i].y = kofNorm[0] + (model[i].y - MIN_Y)*(kofNorm[1] - kofNorm[0])/(MAX_Y - MIN_Y);
        resMod[i].z = kofNorm[0] + (model[i].z - minCount)*(kofNorm[1] - kofNorm[0])/(maxCount - minCount);
    }
    return resMod;
}
