#ifndef EXECTOPERATION_H
#define EXECTOPERATION_H

#include <iostream>
#define SPIN_CORNER 0.174533  // 10 градусов в радианах
#define MOVING_STEP 70
#define COL_COUNT 20
#define ROW_COUNT 20

enum operationType
{
    getMasPoints,
    spinMod,
    movingMod,
    scalMod
};

enum coordinat
{
    axX,
    axY,
    axZ,
};

enum sides
{
    left,
    right
};

enum toScale
{
    plus,
    minus
};

struct point
{
    int x;
    int y;
    int z;
};

struct sourceDate
{
    std::string fileName;
    point *model;
    coordinat axis;             // ось по которой поворот
    sides side;                 // сторона поворота
    toScale scale;              // маштабирование
    int kofNorm[2];             // диапозон нормировки
};

struct resultDate
{
   point *model;
};

resultDate exectOperation(operationType operation, sourceDate& source);
#endif // EXECTOPERATION_H
