#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGraphicsScene>
#include <qfiledialog.h>
#include <iostream>
#include <QMessageBox>
#include <stdio.h>

sourceDate source;
resultDate result;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->downloadFile,SIGNAL(clicked()),this,SLOT(loadFile()));

    connect(ui->butXSpinLeft,SIGNAL(clicked()),this,SLOT(spinModel()));
    connect(ui->butXSpinRight,SIGNAL(clicked()),this,SLOT(spinModel()));
    connect(ui->butYSpinLeft,SIGNAL(clicked()),this,SLOT(spinModel()));
    connect(ui->butYSpinRight,SIGNAL(clicked()),this,SLOT(spinModel()));
    connect(ui->butZSpinLeft,SIGNAL(clicked()),this,SLOT(spinModel()));
    connect(ui->butZSpinRight,SIGNAL(clicked()),this,SLOT(spinModel()));

    connect(ui->butXMovingLeft,SIGNAL(clicked()),this,SLOT(movingModel()));
    connect(ui->butXMovingRight,SIGNAL(clicked()),this,SLOT(movingModel()));
    connect(ui->butYMovingLeft,SIGNAL(clicked()),this,SLOT(movingModel()));
    connect(ui->butYMovingRight,SIGNAL(clicked()),this,SLOT(movingModel()));
    connect(ui->butZMovingLeft,SIGNAL(clicked()),this,SLOT(movingModel()));
    connect(ui->butZMovingRight,SIGNAL(clicked()),this,SLOT(movingModel()));

    connect(ui->butScaleMinus,SIGNAL(clicked()),this,SLOT(modelToScale()));
    connect(ui->butScalePlus,SIGNAL(clicked()),this,SLOT(modelToScale()));

    ui->butXSpinLeft->setEnabled(false);
    ui->butXSpinRight->setEnabled(false);
    ui->butYSpinLeft->setEnabled(false);
    ui->butYSpinRight->setEnabled(false);
    ui->butZSpinLeft->setEnabled(false);
    ui->butZSpinRight->setEnabled(false);

    ui->butXMovingLeft->setEnabled(false);
    ui->butXMovingRight->setEnabled(false);
    ui->butYMovingLeft->setEnabled(false);
    ui->butYMovingRight->setEnabled(false);
    ui->butZMovingLeft->setEnabled(false);
    ui->butZMovingRight->setEnabled(false);

    ui->butScalePlus->setEnabled(false);
    ui->butScaleMinus->setEnabled(false);

    ui->butXSpinLeft->setCheckable(true);
    ui->butXSpinRight->setCheckable(true);
    ui->butYSpinLeft->setCheckable(true);
    ui->butYSpinRight->setCheckable(true);
    ui->butZSpinLeft->setCheckable(true);
    ui->butZSpinRight->setCheckable(true);

    ui->butXMovingLeft->setCheckable(true);
    ui->butXMovingRight->setCheckable(true);
    ui->butYMovingLeft->setCheckable(true);
    ui->butYMovingRight->setCheckable(true);
    ui->butZMovingLeft->setCheckable(true);
    ui->butZMovingRight->setCheckable(true);

    ui->butScalePlus->setCheckable(true);
    ui->butScaleMinus->setCheckable(true);
}


void MainWindow::drowModel()
{
    QPen penForLine(Qt::black,2);
    QPen penForPoint(Qt::black,5);
    QGraphicsScene* scene = new QGraphicsScene();
    scene->setSceneRect(50,50,800,700);
    ui->graphicsView->setScene(scene);

    for(int i=0; i < ROW_COUNT;i++)
        for(int j=0; j < COL_COUNT - 1;j++)
            scene->addLine(result.model[i*20+j].x,result.model[i*20+j].y,result.model[i*20+j+1].x,result.model[i*20+j+1].y, penForLine);

    for(int i=0; i < ROW_COUNT -1 ;i++)
        for(int j=0; j < COL_COUNT;j++)
            scene->addLine(result.model[i*20+j].x,result.model[i*20+j].y,result.model[i*20+j+20].x,result.model[i*20+j+20].y, penForLine);

    for(int i=0; i < ROW_COUNT;i++)
        for(int j=0; j < COL_COUNT;j++)
            scene->addLine(result.model[i*20+j].x,result.model[i*20+j].y,result.model[i*20+j].x,result.model[i*20+j].y, penForPoint);

}

void MainWindow::loadFile(){
    QString fileName = QFileDialog::getOpenFileName(this,"Open Text file", "/home/sap/Рабочий\ стол/laba4", tr("Text Files (*.csv)"));
    ui->fileName->setText(fileName);
    source.fileName=fileName.toStdString();
    source.kofNorm[0] = ui->minKof->value();
    source.kofNorm[1] = ui->maxKof->value();
    if (source.kofNorm[1] - source.kofNorm[0] < 0 || abs(source.kofNorm[1] - source.kofNorm[0]) < 200)
    {
        QMessageBox::warning(0, "warning", "Диапозон нормировки задан неверно!");
        QMessageBox::information(0, "Info", "Примечание: 1.Первое значение диапозона должно быть меньш второго 2.Диапозон по модулю должет превосходить 200");
    }else
    {
        result = exectOperation(getMasPoints,source);
        drowModel();
        allButtonActivate();
    }
}

void MainWindow::spinModel()
{
    QPushButton *button = (QPushButton *)sender();
    button->setChecked(true);
    source.model = new point[400];
    source.model = result.model;
    if(ui->butXSpinLeft->isChecked())
    {
        source.axis = axX;
        source.side = left;
        result = exectOperation(spinMod,source);
        button->setChecked(false);
    }
    else if (ui->butXSpinRight->isChecked())
    {
        source.axis = axX;
        source.side = right;
        result = exectOperation(spinMod,source);
        button->setChecked(false);
    }
    else if(ui->butYSpinLeft->isChecked())
    {
        source.axis = axY;
        source.side = left;
        result = exectOperation(spinMod,source);
        button->setChecked(false);
    }
    else if(ui->butYSpinRight->isChecked())
    {
        source.axis = axY;
        source.side = right;
        result = exectOperation(spinMod,source);
        button->setChecked(false);
    }
    else if(ui->butZSpinLeft->isChecked())
    {
        source.axis = axZ;
        source.side = left;
        result = exectOperation(spinMod,source);
        button->setChecked(false);
    }
    else if(ui->butZSpinRight->isChecked())
    {
        source.axis = axZ;
        source.side = right;
        result = exectOperation(spinMod,source);
        button->setChecked(false);
    }
    drowModel();
}

void MainWindow::movingModel()
{
    QPushButton *button = (QPushButton *)sender();
    button->setChecked(true);
    source.model = new point[400];
    source.model = result.model;
    if(ui->butXMovingLeft->isChecked())
    {
        source.axis = axX;
        source.side = left;
        result = exectOperation(movingMod,source);
        button->setChecked(false);
    }
    else if (ui->butXMovingRight->isChecked())
    {
        source.axis = axX;
        source.side = right;
        result = exectOperation(movingMod,source);
        button->setChecked(false);
    }else
        if(ui->butYMovingLeft->isChecked())
    {
        source.axis = axY;
        source.side = left;
        result = exectOperation(movingMod,source);
        button->setChecked(false);
    }else
        if(ui->butYMovingRight->isChecked())
    {
        source.axis = axY;
        source.side = right;
        result = exectOperation(movingMod,source);
        button->setChecked(false);
    }else
        if(ui->butZMovingLeft->isChecked())
    {
        source.axis = axZ;
        source.side = left;
        result = exectOperation(movingMod,source);
        button->setChecked(false);
    }else
        if(ui->butZMovingRight->isChecked())
    {
        source.axis = axZ;
        source.side = right;
        result = exectOperation(movingMod,source);
        button->setChecked(false);
    }
    drowModel();
}

void MainWindow::modelToScale()
{
    QPushButton *button = (QPushButton *)sender();
    button->setChecked(true);
    source.model = new point[400];
    source.model = result.model;

    if(ui->butScalePlus->isChecked())
    {
        source.scale=plus;
        result = exectOperation(scalMod,source);
        button->setChecked(false);
    }
    else if(ui->butScaleMinus->isChecked())
    {
        source.scale=minus;
        result = exectOperation(scalMod,source);
        button->setChecked(false);
    }
    drowModel();
}

void MainWindow::allButtonActivate()
{
    ui->butXSpinLeft->setEnabled(true);
    ui->butXSpinRight->setEnabled(true);
    ui->butYSpinLeft->setEnabled(true);
    ui->butYSpinRight->setEnabled(true);
    ui->butZSpinLeft->setEnabled(true);
    ui->butZSpinRight->setEnabled(true);

    ui->butXMovingLeft->setEnabled(true);
    ui->butXMovingRight->setEnabled(true);
    ui->butYMovingLeft->setEnabled(true);
    ui->butYMovingRight->setEnabled(true);
    ui->butZMovingLeft->setEnabled(true);
    ui->butZMovingRight->setEnabled(true);

    ui->butScalePlus->setEnabled(true);
    ui->butScaleMinus->setEnabled(true);
}


MainWindow::~MainWindow()
{
    delete ui;
}

